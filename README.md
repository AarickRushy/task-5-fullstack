## Membangun Blog Sederhana Menggunakan laravel Blade serta Laravel UI


Tujuan : Agar dapat menerapkan fitur blade serta laravel ui ke dalam project

Pada Project kali ini, saya memiliki inisiatif untuk berinovasi pada tugas yang diberikan. 
Yaitu dengan menambahkan beberapa CRUD baru seperti Film, Genre, User dan Cast, dan juga menambahkan beberapa Controller dan Model agar lebih menarik lagi website yang telah saya buat. Dan juga saya telah menambahkan beberapa detail kecil yang menarik.

Dokumentasi bisa dilihat pada link berikut : https://drive.google.com/drive/folders/1ECGNLm-VSaY4gho8eaVDajcWCImKhkBF?usp=share_link

Project ini merupakan hasil buatan saya sendiri, sehingga masih banyak kekurangan pada project ini. 

Oleh karena itu saya sebagai Junior Developer sangat menerima kritik ataupun saran yang diberikan kepada saya.
