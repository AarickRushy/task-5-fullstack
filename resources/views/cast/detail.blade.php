@extends('layout.master')
@section('title')
    Halaman Detail Cast
@endsection

@section('subtitle')
    Detail List Cast
@endsection

@section('content')

<h1 class="text-primary">{{$cast->nama}}</h1>
<h1>{{$cast->umur}}</h1>
<p>{{$cast->bio}}</p>

@endsection