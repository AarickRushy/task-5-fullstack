@extends('layout.master')
@section('title')
    Halaman Edit Film
@endsection

@section('subtitle')
   Edit Film
@endsection

@section('content')

<form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Judul</label>
        <input type="text" class="form-control" name="judul" value="{{$film->judul}}">
        @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Ringkasan Film</label>
        <textarea name="ringkasan" class="form-control" cols="30" rows="10">{{$film->ringkasan}}</textarea>
        @error('ringkasan')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Tahun</label>
        <input type="number" class="form-control" name="tahun" value="{{$film->tahun}}">
        @error('tahun')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Poster</label>
        <input type="file" class="form-control" name="poster" placeholder="Masukkan Judul Poster">
        @error('poster')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Genre</label>
        <select name="genre_id" class="form-control" id="">
            <option value="">--Pilih Genre--</option>
            @forelse ($genre as $item)
            @if ($item->id === $film->genre_id)
                <option value="{{$item->id}}" selected>{{$item->nama}}</option>
            @else
                <option value="{{$item->id}}"> {{$item->nama}} </option>
            @endif
                
            @empty
                <option value="">Tidak Ada Data Genre</option>
            @endforelse
        </select>
        @error('poster')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>

@endsection