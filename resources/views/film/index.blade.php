@extends('layout.master')
@section('title')
    Halaman List Film
@endsection

@section('subtitle')
   List Film
@endsection

@section('content')

<a href="/film/create" class="btn btn-primary btn-sm mb-3">Tambah</a>

<div class="row">
        @forelse ($film as $item)
            <div class="col-4">
                <div class="card">
                    <img src="{{asset('image/' . $item->poster)}}" class="card-img-top" alt="..." height="350px">
                    <div class="card-body">
                        <h2><strong>{{$item->tahun}}</strong></h2>
                        <h3>{{$item->judul}}</h3>
                        <p class="card-text">{{ Str::limit($item->ringkasan, 250) }}</p>
                        <a href="/film/{{$item->id}}" class="btn btn-secondary btn-block btn-sm">Detail Film</a>
                        <div class="row my-2">
                            <div class="col">
                                <a href="/film/{{$item->id}}/edit" class="btn btn-danger btn-block btn-sm">Edit</a>
                            </div>
                            <div class="col">
                                <form action="/film/{{$item->id}}" method="POST">
                                    @csrf
                                    @method('delete')
                                        <input type="submit" value="Delete" class="btn btn-warning btn-block btn-sm" value="Delete">
                                </form>
                            </div>
                        </div>                
                    </div>
                </div>
            </div>
        @empty
            <h2>Tidak ada Postingan</h2>
        @endforelse              
</div>

@endsection