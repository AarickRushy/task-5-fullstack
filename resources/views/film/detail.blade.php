@extends('layout.master')
@section('title')
    Halaman Detail Film
@endsection

@section('subtitle')
   Detail Film
@endsection

@section('content')

        <img src="{{asset('image/' . $film->poster)}}" class="card-img-top" alt="...">
        <h2><strong>{{$film->tahun}}</strong></h2>    
        <h3>{{$film->judul}}</h3>
            <p class="card-text">{{$film->ringkasan}}</p>
            <a href="/film" class="btn btn-secondary btn-block btn-sm">Kembali</a>

@endsection