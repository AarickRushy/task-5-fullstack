@extends('layout.master')
@section('title')
    Halaman Home
@endsection

@section('subtitle')
    Blog Mini
@endsection

@section('content')
    Sosial Media Developer Santai Berkualitas<br>
    Belajar dan Berbagi agar hidup ini semakin berkualitas
    Benefit Join di Blog Mini saya
    <ul>
        <li>Mendapatkan motivasi dari sesama developer</li>
        <li>Sharing knowledge dari para mastah Sanber</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    Cara bergabung ke SanberBook
    <ol>
        <li>Mengunjungi website ini</li>
        <li>Mendaftar di <a href="/register">Form Sign Up</a></li>
        <li>Selesai!</li>
    </ol>

@endsection
    