@extends('layout.master')
@section('title')
    Halaman Tambah Genre
@endsection

@section('subtitle')
   Tambah Genre
@endsection

@section('content')

<form action="/genre" method="POST">
    @csrf
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama Genre">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>

@endsection