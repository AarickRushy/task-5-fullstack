<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\FilmController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);
Route::get('/register', [AuthController::class, 'auth']); //nama fungsinya auth
Route::post('/submit', [AuthController::class, 'dashboard']);

Route::get('/data-table', function(){
    return view('page.data-table');
});
Route::get('/table', function(){
    return view('page.table');
});

//CRUD CAST
//CREATE DATA CAST MENGGUNAKAN QUERY BUILDER
Route::get('/cast/create', [CastController::class, 'create']);
Route::post('/cast', [CastController::class, 'store']);
// READ DATA CAST
Route::get('/cast', [CastController::class, 'index']);
Route::get('/cast/{id}', [CastController::class, 'show']);
//UPDATE CAST
Route::get('/cast/{id}/edit', [CastController::class, "edit"]);
Route::put('/cast/{id}', [CastController::class, "update"]);
//DELETE CAST
Route::delete('/cast/{id}', [CastController::class, "destroy"]);

//CRUD GENRE
Route::resource('genre', GenreController::class);
//CRUD FILM
Route::resource('film', FilmController::class);