<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function auth()
    {
        return view('page.register');
    }

    public function dashboard(Request $request)
    {
        $namaDepan = $request['firstName'];
        $namaBelakang = $request['lastName'];
        // $password = $request['password'];
        // $gender = $request['gender'];
        // $nationality = $request['nationality'];
        // $languangeSpoken = $request['languange_spoken'];
        // $bio = $request['bio'];

        return view('page.dashboard', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
